package org.fridlund.wad.tiitinenlist;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;

public interface TiitinenListService {

    @PreAuthorize("hasAnyRole('admin,supo')")
    ListItem create(String content);

    @PreAuthorize("hasRole('admin')")
    void remove(Long identifier);

    @PreAuthorize("isAuthenticated()")
    List<ListItem> list();
}
